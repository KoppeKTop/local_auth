# -*- coding: utf-8 -*-
import sqlite3
from contextlib import closing
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash, jsonify

# configuration
DEBUG = True
if DEBUG:
    DATABASE = '/tmp/local_auth.db'
    SECRET_KEY = 'development key'
else:
    DATABASE = '/var/local_auth/production.db'

USERNAME = 'admin'
PASSWORD = 'default'

app = Flask(__name__)
app.config.from_object(__name__)

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

def valid_ip(ip_addr):
    splitted = ip_addr.split('.')
    result = len(splitted) == 4 and all([0 <= int(el) < 256 for el in splitted])
    return result

@app.route('/new_user', methods=['POST'])
def new_user():
    if not request.json:
        abort(400)
    if not 'tel' in request.json or not 'ip_addr' in request.json:
        abort(400)
    ip_addr = request.json['ip_addr']
    if not valid_ip(ip_addr):
        abort(400)
    tel = request.json['tel']
    cur = g.db.execute('select * from entries where ip_addr = ?', [ip_addr])
    rv = cur.fetchone()
    cur.close()
    if (rv):
        g.db.execute('update entries set tel = ? where id = ?', [tel, rv[0]])
    else:
        g.db.execute('insert into entries (ip_addr, tel) values (?, ?)', [ip_addr, tel])
    g.db.commit()
    return jsonify({'status': 'User added'})

@app.route('/get_phone', methods=['GET'])
def get_phone():
    ip_addr = request.remote_addr
    cur = g.db.execute('select tel from entries where ip_addr = ?', [ip_addr])
    el = cur.fetchone()
    cur.close()
    if el:
        tel = el[0]
        status = "OK"
    else:
        tel = None
        status = "Your IP not found %s" % ip_addr
    return jsonify({'tel': tel, 'status': status})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)


