drop table if exists entries;
create table entries (
  id integer primary key autoincrement,
  ip_addr text not null,
  tel text
);
